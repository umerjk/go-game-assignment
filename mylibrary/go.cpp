#include "go.h"
#include "randomai.h"
#include<memory>
#include<algorithm>


namespace mylib {
namespace go {



  namespace priv {

    Board_base::Board_base( Size size ) {

      resetBoard(size);
    }

    Board_base::Board_base(Board::BoardData&& data, StoneColor turn, bool was_previous_pass)
      : _current{std::forward<Board::BoardData>(data),turn,was_previous_pass}
    {
      // ... init ...
    }

    Board_base::Position::Position(Board::BoardData&& data, StoneColor trn, bool prev_pass)
      : board{data}, turn{trn}, was_previous_pass{prev_pass} {}


    void
    Board_base::resetBoard(Size size) {

      _current.board.clear();
      _size = size;
      _current.turn = StoneColor::Black;
    }

    Size
    Board_base::size() const {

      return _size;
    }

    bool
    Board_base::wasPreviousPass() const {

      return _current.was_previous_pass;
    }

    StoneColor
    Board_base::turn() const {

      return _current.turn == StoneColor::Black ? StoneColor::White : StoneColor::Black;
    }

  }


  void Board::placeStone(Point intersection)
  {
      if(!hasStone(intersection))
      {
          _current.board[intersection] = stone_child{intersection, _current.turn, std::shared_ptr<Board>(this) };
          _current.turn = turn();

//          std::set<Point> newBlock_point = {intersection};
//          Block newBlock = Block(newBlock_point, std::shared_ptr<Board> (this), _current.turn);
//          _current.blocks.insert(std::make_shared<Block> (newBlock));

      }
  }

//  void Board::placeStone(Point intersection)
//  {
//      stone_child b = stone_child(intersection, _current.turn, std::shared_ptr<Board> (this));
//      //Point p = Point(first,second);
//      if(!hasStone(intersection))
//      {
//          //auto s = (intersection);
//          if(!hasStone(b.East()) )//&& !b.has_North() && !b.has_South() && !b.has_West())
//          {
//              _current.board[intersection] = turn();
//              _current.turn = turn();
//              /*Point a = b.East();
//              if(Board::stone(intersection) == Board::stone(a))
//              {*/

//              //}
//          }

//      }
//      //Block::_stone.push_back(intersection);
//  }


  //---------------------------------------------------------------------------//
//  bool stone_child::has_East() const
//  {
//    return (*(this->_board)).hasStone(Point(_point.first+1,_point.second));
//  }

//  Point stone_child::East() const
//  {
//    return Point(_point.first+1,_point.second);
//  }
  //---------------------------------------------------------------------------------//


  void
  Board::passTurn() {

    _current.turn = turn();
  }

  bool
  Board::hasStone(Point intersection) const {

    return _current.board.count(intersection);
  }

//  StoneColor
//  Board::stone(Point intersection) const {

//    return _current.board.at(intersection);
//  }


  bool
  Board::isNextPositionValid(Point /*intersection*/) const {

    return true;
  }

  Engine::Engine()
    : _board{}, _game_mode{}, _active_game{false},
      _white_player{nullptr}, _black_player{nullptr} {}

  void
  Engine::newGame(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameVsAi(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::VsAi;
    _active_game = true;

    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),   StoneColor::White);
  }

  void
  Engine::newGameAiVsAi(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::Ai;
    _active_game = true;

    _white_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameFromState(Board::BoardData&& board, StoneColor turn, bool was_previous_pass) {

    _board = Board {std::forward<Board::BoardData>(board),turn,was_previous_pass};

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  const Board&
  Engine::board() const {

    return _board;
  }

  const GameMode&
  Engine::gameMode() const {

    return _game_mode;
  }

  StoneColor
  Engine::turn() const {

    return _board.turn();
  }

  const std::shared_ptr<Player>
  Engine::currentPlayer() const {

    if(      turn() == StoneColor::Black ) return _black_player;
    else if( turn() == StoneColor::White ) return _white_player;
    else                              return nullptr;
  }

  void
  Engine::placeStone(Point intersection) {

    _board.placeStone(intersection);
  }

  void
  Engine::passTurn() {

    if(board().wasPreviousPass()) {
      _active_game = false;
      return;
    }

    _board.passTurn();
  }

  void
  Engine::nextTurn(std::chrono::duration<int,std::milli> think_time) {

    if( currentPlayer()->type() != PlayerType::Ai) return;

    auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());

    p->think( think_time );
    if( p->nextMove() == AiPlayer::Move::PlaceStone )
      placeStone( p->nextStone() );
    else
      passTurn();
  }

  bool
  Engine::isGameActive() const {

    return _active_game;
  }

  bool
  Engine::validateStone(Point pos) const {

    return _board.isNextPositionValid(pos);
  }


  //-------------------------------PRIV BEGIN----------------------------//


namespace priv
{


//--------------Stone_base----------------------------------//


  bool Stone_Base::has_North() const
  {
    return (*(this->_board)).hasStone(North());
  }

  bool Stone_Base::has_South() const
  {
    return (*(this->_board)).hasStone(South());
  }

  bool Stone_Base::has_East() const
  {
    return (*(this->_board)).hasStone(East());
  }

  bool Stone_Base::has_West() const
  {
    return (*(this->_board)).hasStone(West());
  }

  Point Stone_Base::North() const
  {
    return Point(_point.first,_point.second+1);
  }

  Point Stone_Base::South() const
  {
    return Point(_point.first,_point.second-1);
  }

  Point Stone_Base::East() const
  {
    return Point(_point.first+1,_point.second);
  }

  Point Stone_Base::West() const
  {
    return Point(_point.first-1,_point.second);
  }

  Stone_Base::Stone_Base(Point _point, StoneColor _colour, std::shared_ptr<Board> _board)
  {
    this->_point = _point;
    this->_colour = _colour;
    this->_board = _board;
  }




}//END of PRIV

//-------------------------------PRIV END----------------------------//



//-------------------------------Block--------------------------------------------------------------//


  Block::Block(Point _block, std::shared_ptr<Board> _board, StoneColor _color)
  {
    this->_block.insert( _block);
    this->_board = _board;
    this->_color = _color;

    Boundary newPoint = Boundary {std::vector<Point> {this->board().stones(_block).North(),
                                                      this->board().stones(_block).East(),
                                                      this->board().stones(_block).West(),
                                                      this->board().stones(_block).South() } };
    //adding created vector
    this->_boundaries.insert(std::make_shared<Boundary> (newPoint));
  }

  //constructor for set of blocks
  Block::Block(std::set<Point> _block, std::shared_ptr<Board> _board, StoneColor _color, std::set<std::shared_ptr<Boundary>> _boundaries )
  {
      this->_block = _block;
      this->_board = _board;
      this->_color = _color;
      this->_boundaries = _boundaries;
  }

  void Block::addBlockPoints(std::set<Point> _points)
  {
    this->_block.insert(_points.begin(), _points.end());
  }


  //---------------------------------------------------------------------------------------------------------//

  StoneColor Block::color() const
  {
    return this->_color;
  }

  Board Block::board() const
  {
    return *(this->_board);
  }

  std::set<Point> Block::getBlock() const
  {
    return this->_block;
  }





bool Board::isOutOfBounds(Point intersection) const
  {
      if ( intersection.first > this->size().first || intersection.second > this->size().second )
      {
          return true;
      }
      else
      {
          return false;
      }
  }


stone_child Board::stones(Point intersection) const
{
    if (this->hasStone(intersection))
    {
        return _current.board.at( intersection );
    }
}



//-------------------Boundary-------------------------------------------------//


Boundary::Boundary(std::vector<Point> _points)
{
    this->_points = _points;
}

std::vector<Point> Boundary::getPoints()
{

}

void Boundary::replaceAt(Point replacePoint, std::vector<Point> _points)
{
    auto findIterator = std::find(this->_points.begin(), this->_points.end(), replacePoint);

    //check if findIteratoe found point
    //it will return end if it doesnt found any point
    if(findIterator != this->_points.end())
    {
        this->_points.insert(findIterator, _points.begin(), _points.end());
        this->_points.erase(findIterator);
    }
}

//std::set<Point> Board::gets_adjacentSameColor(stone_child intersection) const
//{
//    std::set<Point> returnset;
//    if(!this->isOutOfBounds(intersection.North()))
//    {
//        if(this->hasStone(intersection.North()))
//        {
//            if( intersection.color() == this->stones( intersection.North() ).color() )
//            {
//                returnset.insert( intersection.North() );
//            }
//        }
//    }
//}





/*Block::Block(std::shared_ptr<stone_child> stone_position)
{
    _stone.push_back();
}*/









} // END namespace go
} // END namespace mylib
